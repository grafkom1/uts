# UTS
Kelompok: GitLab Enjoyer  
Anggota:
1. Faza Aulia Prayoga - 1806173525
2. Muhammad Tsaqif Al Bari - 1806235731
3. Naufal Alauddin Hilmi - 1806205754

Soal 1: Charge Attack!  
Sebuah animasi 2D interaktif dimana user dapat melakukan sebuah "Charge Attack" dengan spamming key C pada keyboard.  
Bisa juga diakses pada link berikut: [https://grafkom1.gitlab.io/uts/Soal%201/chargeAttack.html](https://grafkom1.gitlab.io/uts/Soal%201/chargeAttack.html)

Kontribusi:
1. Muhammad Tsaqif Al Bari (100%)

Soal 2: Implementasi Algoritma Midpointline
Menggambar garis menggunakan poin-poin yang digenerate menggunakan algoritma midpoint.
Algoritma midpoint yang digunakan dapat membuat garis dengan semua kasus slope.

Petunjuk penggunaan:
1. Klik 1 kali dikanvas untuk meletakkan koordinat pertama.
2. Klik 1 kali lagi dikanvas untuk meletakkan koordinat kedua lalu garis akan segera di render.

Kontribusi:
1. Faza Aulia Aryoga (100%)

Soal 3: Simulasi H₂O  
Animasi yang menggambarkan dua objek mengitari sebuah objek yang lebih besar. Dilengkapi dengan dropdown untuk mengganti camera antara:
- Default
- Internal Oxygen
- Internal Hydrogen-1 (menghadap oxygen)
- Internal Hydrogen-2 (menghadap oxygen)  

Dilengkapi juga dengan opsi *smoothness* untuk memilih antara transisi yang lebih halus atau loncat.  
Bisa juga diakses pada link berikut: https://grafkom1.gitlab.io/uts/soal%203/h2o.html

Kontribusi:
1. Naufal Alauddin Hilmi (100%)

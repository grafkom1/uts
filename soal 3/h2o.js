"use strict";

var start;

var rotationAngle = 0;
var revolutionAngle = 0;
const zRevolutionAngleStart = 30/180*Math.PI;
var zRevolutionAngle = zRevolutionAngleStart;

const radius = 300;

const rotationSpeed = 3 * Math.PI;
const revolutionSpeed = 1/3 *Math.PI;
const zRevolutionSpeed = revolutionSpeed/12;

const hydrogenScale = 1/4;

const FOV = 90/180*Math.PI;
var aspectRatio;
const near = 1;
const far = 4000;

var camera = "default";
var smoothTransition = true;
var pointAtOxygen = true;
var stayUpRight = true;
const numberOfTriangle = 24;

window.onload = function init() {
    const canvas = document.getElementById("canvas");
    const gl = canvas.getContext("webgl2");
    if (!gl) {
        alert("WebGL 2.0 isn't available");
        return;
    }

    gl.viewport(0, 0, canvas.width, canvas.height);
    gl.clearColor(0.0, 0.0, 0.0, 1.0);

    // enable depth buffer
    gl.enable(gl.CULL_FACE);
    gl.enable(gl.DEPTH_TEST);

    //  Load shaders and initialize attribute buffers
    const program = initShaders(gl, "vertex-shader", "fragment-shader");
    gl.useProgram(program);

    // Load the data into the GPU
    const positionBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);

    const positionLocation = gl.getAttribLocation(program, "position");
    gl.vertexAttribPointer(positionLocation, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(positionLocation);

    setGeometry(gl);

    const colorBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer);

    const colorLocation = gl.getAttribLocation(program, "color");
    gl.vertexAttribPointer(colorLocation, 3, gl.UNSIGNED_BYTE, true, 0, 0);
    gl.enableVertexAttribArray(colorLocation);

    setColor(gl);

    const matrixLocation = gl.getUniformLocation(program, "transformation");

    document.getElementById("camera").addEventListener("change", function onCameraChange(event) {
        camera = event.target.value;
    });

    const smoothOption = document.getElementById("smooth")
    smoothOption.addEventListener("change", function onSmoothChange(event) {
        smoothTransition = smoothOption.checked;
    });
    smoothTransition = smoothOption.checked;

    const pointAtOxygenOption = document.getElementById("direction")
    pointAtOxygenOption.addEventListener("change", function onDirectionChange(event) {
        pointAtOxygen = pointAtOxygenOption.checked;
    });
    pointAtOxygen = pointAtOxygenOption.checked;

    const stayUpRightOption = document.getElementById("up-right")
    stayUpRightOption.addEventListener("change", function onStayUprightChange(event) {
        stayUpRight = stayUpRightOption.checked;
    })
    stayUpRight = stayUpRightOption.checked;

    aspectRatio = canvas.width / canvas.height;

    requestAnimationFrame((currentTime) => render(currentTime, gl, matrixLocation));
}

function render(currentTime, gl, matrixLocation) {
    if (!start) {
        start = currentTime;
    }
    
    const elapsedTime = currentTime - start;

    rotationAngle = rotationSpeed * elapsedTime / 1000;
    revolutionAngle = revolutionSpeed * elapsedTime / 1000;

    let zRevolution = zRevolutionSpeed * elapsedTime / 1000;

    if (!smoothTransition) {
        zRevolution = Math.floor(zRevolution*12/(2*Math.PI))*Math.PI/6;
    }
    
    zRevolutionAngle =  zRevolutionAngleStart + zRevolution;
    
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    const [hydrogen1PositionMatrix, balancedH1] = hydrogen1Transformation();
    const [hydrogen2PositionMatrix, balancedH2] = hydrogen2Transformation();

    const cameraMatrix = createCameraMatrix(hydrogen1PositionMatrix, balancedH1, hydrogen2PositionMatrix, balancedH2);

    drawOxygen(gl, matrixLocation, cameraMatrix);
    drawHydrogen1(gl, matrixLocation, cameraMatrix, balancedH1);
    drawHydrogen2(gl, matrixLocation, cameraMatrix, balancedH2);

    requestAnimationFrame((timeCallback) => render(timeCallback, gl, matrixLocation));
}

function setGeometry(gl) {
    const v1 = [Math.sqrt(8/9), -1/3, 0];
    const v2 = [-Math.sqrt(2/9), -1/3, Math.sqrt(2/3)];
    const v3 = [-Math.sqrt(2/9), -1/3, -Math.sqrt(2/3)];
    const v4 = [0, 1, 0];
    const tetrahedron = [
        v1, v2, v3,
        v2, v4, v3,
        v3, v4, v1,
        v1, v4, v2
    ]
    const lowerTetrahedron = tetrahedron.map(l => l.map(n => -n)).reverse();;
    
    const star = new Float32Array(
        tetrahedron.concat(lowerTetrahedron).flat().map(n => n * 100)
    );
    gl.bufferData(gl.ARRAY_BUFFER, star, gl.STATIC_DRAW)
}

function setColor(gl) {
    const tetrahedronColor = new Uint8Array([
        // upper tetrahedron
        0, 0, 255,
        0, 0, 255,
        0, 0, 255,
        0, 255, 0,
        0, 255, 0,
        0, 255, 0,
        255, 0, 0,
        255, 0, 0,
        255, 0, 0,
        255, 127, 0,
        255, 127, 0,
        255, 127, 0,

        // lower tetrahedron
        0, 255, 255,
        0, 255, 255,
        0, 255, 255,
        255, 255, 0,
        255, 255, 0,
        255, 255, 0,
        255, 0, 255,
        255, 0, 255,
        255, 0, 255,
        127, 255, 0,
        127, 255, 0,
        127, 255, 0,

    ]);
    gl.bufferData(gl.ARRAY_BUFFER, tetrahedronColor, gl.STATIC_DRAW);
}

function createCameraMatrix(hydrogen1PositionMatrix, balancedH1Matrix, hydrogen2PositionMatrix, balancedH2Matrix) {
    let matrix = m4.identity();
    let cameraPosition = [0, 0, 1, 1];
    let target = [0, 0, 0, 1];
    let up = [0, 1, 0, 1];
    
    switch (camera) {
        case "default":
            matrix = m4.translate(matrix, 0, 0, 500);
            break;
        case "oxygen":
            matrix = m4.identity();
            break;
        case "h1":
            if (pointAtOxygen) {
                cameraPosition = m4.vectorMultiply(hydrogen1PositionMatrix, cameraPosition);
                up = m4.vectorMultiply(hydrogen1PositionMatrix, up);
                matrix = m4.lookAt(cameraPosition, target, up);
            } else {
                matrix = balancedH1Matrix;
            }

            break;
        case "h2":
            if (pointAtOxygen) {
                cameraPosition = m4.vectorMultiply(hydrogen2PositionMatrix, cameraPosition);
                up = m4.vectorMultiply(hydrogen2PositionMatrix, up);
                matrix = m4.lookAt(cameraPosition, target, up);
            } else {
                matrix = balancedH2Matrix;
            }
            break;
    }

    return matrix;
}

function drawOxygen(gl, matrixLocation, cameraMatrix) {
    let matrix = m4.identity();

    // project to camera
    matrix = m4.perspective(FOV, aspectRatio, near, far);
    matrix = m4.multiply(matrix, m4.inverse(cameraMatrix));

    // rotate hydrogen
    matrix = m4.yRotate(matrix, rotationAngle);

    // Set the matrix.
    gl.uniformMatrix4fv(matrixLocation, false, matrix);

    gl.drawArrays(gl.TRIANGLES, 0, numberOfTriangle);
}

function hydrogen1Transformation() {
    let matrix = m4.identity();

    // revolve the hydrogen
    let revolutionMatrix = m4.zRotate(matrix, zRevolutionAngle);
    revolutionMatrix = m4.yRotate(revolutionMatrix, revolutionAngle);
    
    // translate the hydrogen
    matrix = m4.translate(revolutionMatrix, radius, 0, 0);
    
    // balance the hydrogen back to upright position
    let balancedMatrix = matrix; 
    if (stayUpRight) balancedMatrix = m4.multiply(matrix, m4.inverse(revolutionMatrix));

    return [matrix, balancedMatrix];
}

function drawHydrogen1(gl, matrixLocation, cameraMatrix, transformation) {
    let matrix = m4.identity();

    // project
    matrix = m4.perspective(FOV, aspectRatio, near, far);
    matrix = m4.multiply(matrix, m4.inverse(cameraMatrix));
    
    // put to position
    matrix = m4.multiply(matrix, transformation);
    
    // shrink and rotate
    matrix = m4.uniformScale(matrix, hydrogenScale);
    matrix = m4.yRotate(matrix, rotationAngle);

    // Set the matrix.
    gl.uniformMatrix4fv(matrixLocation, false, matrix);

    gl.drawArrays(gl.TRIANGLES, 0, numberOfTriangle);
}

function hydrogen2Transformation() {
    let matrix = m4.identity();
    
    // revolve
    let revolutionMatrix = m4.zRotate(matrix, -zRevolutionAngle);
    revolutionMatrix = m4.yRotate(revolutionMatrix, -revolutionAngle);
    
    // displace the hydrogen
    matrix = m4.translate(revolutionMatrix, -radius, 0, 0);
    
    // balance the hydrogen back to upright position
    let balancedMatrix = matrix;
    if (stayUpRight) balancedMatrix = m4.multiply(matrix, m4.inverse(revolutionMatrix));

    return [matrix, balancedMatrix];
}

function drawHydrogen2(gl, matrixLocation, cameraMatrix, transformation) {
    let matrix = m4.identity();

    // project
    matrix = m4.perspective(FOV, aspectRatio, near, far);
    matrix = m4.multiply(matrix, m4.inverse(cameraMatrix));

    // put to position
    matrix = m4.multiply(matrix, transformation)
    
    // shrink and rotate
    matrix = m4.uniformScale(matrix, hydrogenScale);
    matrix = m4.yRotate(matrix, rotationAngle);

    // Set the matrix.
    gl.uniformMatrix4fv(matrixLocation, false, matrix);

    gl.drawArrays(gl.TRIANGLES, 0, numberOfTriangle);
}



// ----------------
// Utility function
// ----------------

function subtractVectors(a, b) {
    return [a[0] - b[0], a[1] - b[1], a[2] - b[2]];
}

function normalize(v) {
    var length = Math.sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
    // make sure we don't divide by 0.
    if (length > 0.00001) {
        return [v[0] / length, v[1] / length, v[2] / length];
    } else {
        return [0, 0, 0];
    }
}

function cross(a, b){
    return [a[1] * b[2] - a[2] * b[1],
            a[2] * b[0] - a[0] * b[2],
            a[0] * b[1] - a[1] * b[0]];
  }
  
const m4 = {
    lookAt: function(cameraPosition, target, up){
        var zAxis = normalize(subtractVectors(cameraPosition, target));
        var xAxis = cross(up, zAxis);
        var yAxis = cross(zAxis, xAxis);
    
        return [
           xAxis[0], xAxis[1], xAxis[2], 0,
           yAxis[0], yAxis[1], yAxis[2], 0,
           zAxis[0], zAxis[1], zAxis[2], 0,
           cameraPosition[0],
           cameraPosition[1],
           cameraPosition[2],
           1,
        ];
      },

    perspective: function(fieldOfViewInRadians, aspect, near, far){
        var f = Math.tan(Math.PI * 0.5 - 0.5 * fieldOfViewInRadians);
        var rangeInv = 1.0 / (near - far);

        return [
            f / aspect, 0, 0, 0,
            0, f, 0, 0,
            0, 0, (near + far) * rangeInv, -1,
            0, 0, near * far * rangeInv * 2, 0
        ];
    },

    multiply: function(a, b){
        var a00 = a[0 * 4 + 0];
        var a01 = a[0 * 4 + 1];
        var a02 = a[0 * 4 + 2];
        var a03 = a[0 * 4 + 3];
        var a10 = a[1 * 4 + 0];
        var a11 = a[1 * 4 + 1];
        var a12 = a[1 * 4 + 2];
        var a13 = a[1 * 4 + 3];
        var a20 = a[2 * 4 + 0];
        var a21 = a[2 * 4 + 1];
        var a22 = a[2 * 4 + 2];
        var a23 = a[2 * 4 + 3];
        var a30 = a[3 * 4 + 0];
        var a31 = a[3 * 4 + 1];
        var a32 = a[3 * 4 + 2];
        var a33 = a[3 * 4 + 3];
        var b00 = b[0 * 4 + 0];
        var b01 = b[0 * 4 + 1];
        var b02 = b[0 * 4 + 2];
        var b03 = b[0 * 4 + 3];
        var b10 = b[1 * 4 + 0];
        var b11 = b[1 * 4 + 1];
        var b12 = b[1 * 4 + 2];
        var b13 = b[1 * 4 + 3];
        var b20 = b[2 * 4 + 0];
        var b21 = b[2 * 4 + 1];
        var b22 = b[2 * 4 + 2];
        var b23 = b[2 * 4 + 3];
        var b30 = b[3 * 4 + 0];
        var b31 = b[3 * 4 + 1];
        var b32 = b[3 * 4 + 2];
        var b33 = b[3 * 4 + 3];
        return [
            b00 * a00 + b01 * a10 + b02 * a20 + b03 * a30,
            b00 * a01 + b01 * a11 + b02 * a21 + b03 * a31,
            b00 * a02 + b01 * a12 + b02 * a22 + b03 * a32,
            b00 * a03 + b01 * a13 + b02 * a23 + b03 * a33,
            b10 * a00 + b11 * a10 + b12 * a20 + b13 * a30,
            b10 * a01 + b11 * a11 + b12 * a21 + b13 * a31,
            b10 * a02 + b11 * a12 + b12 * a22 + b13 * a32,
            b10 * a03 + b11 * a13 + b12 * a23 + b13 * a33,
            b20 * a00 + b21 * a10 + b22 * a20 + b23 * a30,
            b20 * a01 + b21 * a11 + b22 * a21 + b23 * a31,
            b20 * a02 + b21 * a12 + b22 * a22 + b23 * a32,
            b20 * a03 + b21 * a13 + b22 * a23 + b23 * a33,
            b30 * a00 + b31 * a10 + b32 * a20 + b33 * a30,
            b30 * a01 + b31 * a11 + b32 * a21 + b33 * a31,
            b30 * a02 + b31 * a12 + b32 * a22 + b33 * a32,
            b30 * a03 + b31 * a13 + b32 * a23 + b33 * a33,
        ];
    },

    vectorMultiply(m, v) {
        var m00 = m[0 * 4 + 0];
        var m01 = m[0 * 4 + 1];
        var m02 = m[0 * 4 + 2];
        var m03 = m[0 * 4 + 3];
        var m10 = m[1 * 4 + 0];
        var m11 = m[1 * 4 + 1];
        var m12 = m[1 * 4 + 2];
        var m13 = m[1 * 4 + 3];
        var m20 = m[2 * 4 + 0];
        var m21 = m[2 * 4 + 1];
        var m22 = m[2 * 4 + 2];
        var m23 = m[2 * 4 + 3];
        var m30 = m[3 * 4 + 0];
        var m31 = m[3 * 4 + 1];
        var m32 = m[3 * 4 + 2];
        var m33 = m[3 * 4 + 3];
        return [
            m00 * v[0] + m10 * v[1] + m20*v[2] + m30*v[3],
            m01 * v[0] + m11 * v[1] + m21*v[2] + m31*v[3],
            m02 * v[0] + m12 * v[1] + m22*v[2] + m32*v[3],
            m03 * v[0] + m13 * v[1] + m23*v[2] + m33*v[3],
        ];
    },

    projection: function(width, height, depth) {
        // Note: This matrix flips the Y axis so 0 is at the top.
        return [
           2 / width, 0, 0, 0,
           0, -2 / height, 0, 0,
           0, 0, 2 / depth, 0,
          -1, 1, 0, 1,
        ];
      },

    translation: function(tx, ty, tz){
        return [
            1,  0,  0,  0,
            0,  1,  0,  0,
            0,  0,  1,  0,
            tx, ty, tz, 1,
        ];
    },

    xRotation: function(angleInRadians){
        var c = Math.cos(angleInRadians);
        var s = Math.sin(angleInRadians);

        return [
            1, 0, 0, 0,
            0, c, s, 0,
            0, -s, c, 0,
            0, 0, 0, 1,
        ];
    },

    yRotation: function(angleInRadians){
        var c = Math.cos(angleInRadians);
        var s = Math.sin(angleInRadians);

        return [
            c, 0, -s, 0,
            0, 1, 0, 0,
            s, 0, c, 0,
            0, 0, 0, 1,
        ];
    },

    zRotation: function(angleInRadians){
        var c = Math.cos(angleInRadians);
        var s = Math.sin(angleInRadians);

        return [
            c, s, 0, 0,
            -s, c, 0, 0,
            0, 0, 1, 0,
            0, 0, 0, 1,
        ];
    },

    inverse: function(m){
        var m00 = m[0 * 4 + 0];
        var m01 = m[0 * 4 + 1];
        var m02 = m[0 * 4 + 2];
        var m03 = m[0 * 4 + 3];
        var m10 = m[1 * 4 + 0];
        var m11 = m[1 * 4 + 1];
        var m12 = m[1 * 4 + 2];
        var m13 = m[1 * 4 + 3];
        var m20 = m[2 * 4 + 0];
        var m21 = m[2 * 4 + 1];
        var m22 = m[2 * 4 + 2];
        var m23 = m[2 * 4 + 3];
        var m30 = m[3 * 4 + 0];
        var m31 = m[3 * 4 + 1];
        var m32 = m[3 * 4 + 2];
        var m33 = m[3 * 4 + 3];
        var tmp_0  = m22 * m33;
        var tmp_1  = m32 * m23;
        var tmp_2  = m12 * m33;
        var tmp_3  = m32 * m13;
        var tmp_4  = m12 * m23;
        var tmp_5  = m22 * m13;
        var tmp_6  = m02 * m33;
        var tmp_7  = m32 * m03;
        var tmp_8  = m02 * m23;
        var tmp_9  = m22 * m03;
        var tmp_10 = m02 * m13;
        var tmp_11 = m12 * m03;
        var tmp_12 = m20 * m31;
        var tmp_13 = m30 * m21;
        var tmp_14 = m10 * m31;
        var tmp_15 = m30 * m11;
        var tmp_16 = m10 * m21;
        var tmp_17 = m20 * m11;
        var tmp_18 = m00 * m31;
        var tmp_19 = m30 * m01;
        var tmp_20 = m00 * m21;
        var tmp_21 = m20 * m01;
        var tmp_22 = m00 * m11;
        var tmp_23 = m10 * m01;

        var t0 = (tmp_0 * m11 + tmp_3 * m21 + tmp_4 * m31) -
            (tmp_1 * m11 + tmp_2 * m21 + tmp_5 * m31);
        var t1 = (tmp_1 * m01 + tmp_6 * m21 + tmp_9 * m31) -
            (tmp_0 * m01 + tmp_7 * m21 + tmp_8 * m31);
        var t2 = (tmp_2 * m01 + tmp_7 * m11 + tmp_10 * m31) -
            (tmp_3 * m01 + tmp_6 * m11 + tmp_11 * m31);
        var t3 = (tmp_5 * m01 + tmp_8 * m11 + tmp_11 * m21) -
            (tmp_4 * m01 + tmp_9 * m11 + tmp_10 * m21);

        var d = 1.0 / (m00 * t0 + m10 * t1 + m20 * t2 + m30 * t3);

        return [
            d * t0,
            d * t1,
            d * t2,
            d * t3,
            d * ((tmp_1 * m10 + tmp_2 * m20 + tmp_5 * m30) -
                    (tmp_0 * m10 + tmp_3 * m20 + tmp_4 * m30)),
            d * ((tmp_0 * m00 + tmp_7 * m20 + tmp_8 * m30) -
                    (tmp_1 * m00 + tmp_6 * m20 + tmp_9 * m30)),
            d * ((tmp_3 * m00 + tmp_6 * m10 + tmp_11 * m30) -
                    (tmp_2 * m00 + tmp_7 * m10 + tmp_10 * m30)),
            d * ((tmp_4 * m00 + tmp_9 * m10 + tmp_10 * m20) -
                    (tmp_5 * m00 + tmp_8 * m10 + tmp_11 * m20)),
            d * ((tmp_12 * m13 + tmp_15 * m23 + tmp_16 * m33) -
                    (tmp_13 * m13 + tmp_14 * m23 + tmp_17 * m33)),
            d * ((tmp_13 * m03 + tmp_18 * m23 + tmp_21 * m33) -
                    (tmp_12 * m03 + tmp_19 * m23 + tmp_20 * m33)),
            d * ((tmp_14 * m03 + tmp_19 * m13 + tmp_22 * m33) -
                    (tmp_15 * m03 + tmp_18 * m13 + tmp_23 * m33)),
            d * ((tmp_17 * m03 + tmp_20 * m13 + tmp_23 * m23) -
                    (tmp_16 * m03 + tmp_21 * m13 + tmp_22 * m23)),
            d * ((tmp_14 * m22 + tmp_17 * m32 + tmp_13 * m12) -
                    (tmp_16 * m32 + tmp_12 * m12 + tmp_15 * m22)),
            d * ((tmp_20 * m32 + tmp_12 * m02 + tmp_19 * m22) -
                    (tmp_18 * m22 + tmp_21 * m32 + tmp_13 * m02)),
            d * ((tmp_18 * m12 + tmp_23 * m32 + tmp_15 * m02) -
                    (tmp_22 * m32 + tmp_14 * m02 + tmp_19 * m12)),
            d * ((tmp_22 * m22 + tmp_16 * m02 + tmp_21 * m12) -
                    (tmp_20 * m12 + tmp_23 * m22 + tmp_17 * m02))
        ];
    },

    scaling: function(sx, sy, sz) {
        return [
            sx, 0,  0,  0,
            0, sy,  0,  0,
            0,  0, sz,  0,
            0,  0,  0,  1,
        ];
    },

    uniformScaling: function(scale) {
        return m4.scaling(scale, scale, scale);
    },
    
    translate: function(m, tx, ty, tz) {
        return m4.multiply(m, m4.translation(tx, ty, tz));
    },

    xRotate: function(m, angleInRadians) {
        return m4.multiply(m, m4.xRotation(angleInRadians));
    },

    yRotate: function(m, angleInRadians) {
        return m4.multiply(m, m4.yRotation(angleInRadians));
    },

    zRotate: function(m, angleInRadians) {
        return m4.multiply(m, m4.zRotation(angleInRadians));
    },

    scale: function(m, sx, sy, sz) {
        return m4.multiply(m, m4.scaling(sx, sy, sz));
    },

    uniformScale: function(m, scale) {
        return m4.scale(m, scale, scale, scale);
    },

    project: function(m, width, height, depth) {
        return m4.multiply(m, m4.projection(width, height, depth))
    },

    identity: function() {
        return [
            1, 0, 0, 0,
            0, 1, 0, 0,
            0, 0, 1, 0,
            0, 0, 0, 1,
        ]
    }

};

var gl;
var canvas;
var program;
var bg_color = 0.9; // canvas color
var timeout = 200; // default timeout between render

var key_charge = 0; // count the number of times C key has been pressed
var key_is_pressed = false; // flag to help counting the number of times C key has been pressed
var phase1_upperbound = 10; // the number of times C key has to be pressed to proceed to phase 2
var phase2_upperbound = 30; // the number of times C key has to be pressed to proceed to phase 3
var phase3_upperbound = 50; // the number of times C key has to be pressed to surpassed the final phase
var phase1_scale; // key_charge / phase1_upperbound;
var phase2_scale; // (key_charge - phase1_upperbound) / (phase2_upperbound - phase1_upperbound);
var phase3_scale; // (key_charge - phase2_upperbound) / (phase3_upperbound - phase2_upperbound);

var translation;
var angle = 0;
var angleInRadians = 0;
var scale = [1.0, 1.0]; //  default scale
var matrix;
var matrixLocation;
var translationMatrix;
var rotationMatrix;
var scaleMatrix;
var projectionMatrix;

var middlewidth = 0;
var middleheight = 0;

function drawPhase1() {
  // count phase1_scale, a scale which indicate how much key_charge has
  // compare to phase 1 upperbound,
  // used for animation
  phase1_scale = key_charge / phase1_upperbound;

  // get the needed vertices
  var vertices = setGeometry(1);

  // create a buffer and insert vertices to buffer
  var n = initBuffers(vertices);

  // pinpoint object location and current angle
  translation = [middlewidth, middleheight];
  angleInRadians = 4 * Math.PI * (1 - phase1_scale);

  // create the matrix for projection, translation, rotation and scale
  projectionMatrix = m3.projection(gl.canvas.width, gl.canvas.height);
  translationMatrix = m3.translation(translation[0], translation[1]);
  rotationMatrix = m3.rotation(angleInRadians);
  scaleMatrix = m3.scaling(scale[0], scale[1]);

  // apply those matrix
  matrix = m3.multiply(projectionMatrix, translationMatrix);
  matrix = m3.multiply(matrix, rotationMatrix);
  matrix = m3.multiply(matrix, scaleMatrix);

  // get the color coding
  var colorVertices = setColor(1);

  // create a new buffer for color
  initColorBuffers(colorVertices);

  // set matrix
  gl.uniformMatrix3fv(matrixLocation, false, matrix);

  // draw
  gl.drawArrays(gl.LINE_STRIP, 0, n);
}

function drawPhase2() {
  // count phase2_scale, a scale which indicate how much key_charge has
  // compare to phase 2 upperbound,
  // used for animation
  phase2_scale =
    (key_charge - phase1_upperbound) / (phase2_upperbound - phase1_upperbound);

  // get the needed vertices
  var vertices = setGeometry(2);

  // create a buffer and insert vertices to buffer
  var n = initBuffers(vertices);

  // pinpoint object location and current angle
  translation = [middlewidth, middleheight - 100 * phase2_scale];
  angleInRadians = 32 * Math.PI * (1 - phase2_scale);

  // create the matrix for projection, translation, rotation and scale
  projectionMatrix = m3.projection(gl.canvas.width, gl.canvas.height);
  translationMatrix = m3.translation(translation[0], translation[1]);
  rotationMatrix = m3.rotation(angleInRadians);
  scaleMatrix = m3.scaling(
    scale[0] + 3 * phase2_scale,
    scale[1] + 3 * phase2_scale
  );

  // apply those matrix
  matrix = m3.multiply(projectionMatrix, translationMatrix);
  matrix = m3.multiply(matrix, rotationMatrix);
  matrix = m3.multiply(matrix, scaleMatrix);

  // get the color coding
  var color = setColor(2);

  // create a new buffer for color
  initColorBuffers(color);

  // set the matrix
  gl.uniformMatrix3fv(matrixLocation, false, matrix);

  // draw
  gl.drawArrays(gl.TRIANGLES, 0, n);
}

function drawPhase3() {
  // count phase3_scale, a scale which indicate how much key_charge has
  // compare to phase 3 upperbound,
  // used for animation
  if (key_charge > phase3_upperbound) {
    phase3_scale = 1;
    // count new angle so animation keeps going even after surpassing
    // phase 3 upperbound
    angleInRadians = 64 * Math.PI * (1 - key_charge / 100);
  } else {
    phase3_scale =
      (key_charge - phase2_upperbound) /
      (phase3_upperbound - phase2_upperbound);
    angleInRadians = 64 * Math.PI * (1 - phase3_scale);
  }

  // get the needed vertices
  var vertices = setGeometry(3);

  // create a buffer and insert vertices to buffer
  var n = initBuffers(vertices);

  // pinpoint object location
  translation = [middlewidth, middleheight - 100];

  // create the matrix for projection, translation, rotation and scale
  projectionMatrix = m3.projection(gl.canvas.width, gl.canvas.height);
  translationMatrix = m3.translation(translation[0], translation[1]);
  rotationMatrix = m3.rotation(angleInRadians);
  scaleMatrix = m3.scaling(
    scale[0] + 3 + 2 * phase3_scale,
    scale[1] + 3 + 2 * phase3_scale
  );

  // apply those matrix
  matrix = m3.multiply(projectionMatrix, translationMatrix);
  matrix = m3.multiply(matrix, rotationMatrix);
  matrix = m3.multiply(matrix, scaleMatrix);

  // get the color coding
  var color = setColor(3);

  // create a new buffer for color
  initColorBuffers(color);

  // set the matrix
  gl.uniformMatrix3fv(matrixLocation, false, matrix);

  // draw
  gl.drawArrays(gl.TRIANGLES, 0, n);
}

/* a function that draws background for every phase */
function drawBackground() {
  drawCharacter();
  drawPowerBar();
}

function drawCharacter() {
  // get the needed vertices
  var vertices = setGeometry(0);

  // create a buffer and insert vertices to buffer
  var n = initBuffers(vertices);

  // pinpoint object location
  translation = [middlewidth, middleheight];

  // hardcoded scaling, special case for this draw only
  var scaleUp = 4;

  // creates the matrix for projection, translation and scaling
  projectionMatrix = m3.projection(gl.canvas.width, gl.canvas.height);
  translationMatrix = m3.translation(translation[0], translation[1]);
  scaleMatrix = m3.scaling(scale[0] * scaleUp, scale[1] * scaleUp);

  // apply those matrix
  matrix = m3.multiply(projectionMatrix, translationMatrix);
  matrix = m3.multiply(matrix, scaleMatrix);

  // get the color coding
  var color = setColor(0);

  // create a new buffer for color
  initColorBuffers(color);

  // set the matrix
  gl.uniformMatrix3fv(matrixLocation, false, matrix);

  // draw
  gl.drawArrays(gl.TRIANGLES, 0, n);
}

function drawPowerBar() {
  // since power bar is dynamic, it is counted here rather
  // then using setGeometry
  var vertices = new Float32Array(12);

  // Count power scale, number of key spam / phase 3 upperbound.
  var powerScale;
  if (key_charge < phase3_upperbound) {
    powerScale = key_charge / phase3_upperbound;
  } else {
    powerScale = 1;
  }

  // Count the coords according to number of key_charge to draw
  // the power bar
  var minCoords = [0, middleheight, 20, middleheight];
  //var maxCoords = [80, -middleheight, 0, -middleheight];
  var currentPowerCoords = [
    (80 - 20) * powerScale + 20,
    middleheight * 2 * (0.5 - powerScale),
    0,
    middleheight * 2 * (0.5 - powerScale),
  ];

  // Inserting calculated coords to buffer,
  // There has to be a fancier way to do this
  vertices[0] = minCoords[0];
  vertices[1] = minCoords[1];
  vertices[2] = minCoords[2];
  vertices[3] = minCoords[3];
  vertices[4] = currentPowerCoords[2];
  vertices[5] = currentPowerCoords[3];
  vertices[6] = currentPowerCoords[2];
  vertices[7] = currentPowerCoords[3];
  vertices[8] = minCoords[2];
  vertices[9] = minCoords[3];
  vertices[10] = currentPowerCoords[0];
  vertices[11] = currentPowerCoords[1];
  // inserting to buffer to draw later
  var n = initBuffers(vertices);

  // power bar location
  translation = [middlewidth - 255, middleheight];

  projectionMatrix = m3.projection(gl.canvas.width, gl.canvas.height);
  translationMatrix = m3.translation(translation[0], translation[1]);

  // power bar location on canvas
  matrix = m3.multiply(projectionMatrix, translationMatrix);

  // pick which color is the power bar according to phase
  var color;
  if (key_charge < phase1_upperbound) {
    color = setColor(4);
  } else if (
    key_charge >= phase1_upperbound &&
    key_charge < phase2_upperbound
  ) {
    color = setColor(5);
  } else if (key_charge >= phase2_upperbound) {
    color = setColor(6);
  }
  initColorBuffers(color);

  // set matrix
  gl.uniformMatrix3fv(matrixLocation, false, matrix);

  // draw
  gl.drawArrays(gl.TRIANGLES, 0, n);
}

function init() {
  canvas = document.getElementById("gl-canvas");
  gl = canvas.getContext("webgl2");
  if (!gl) alert("WebGL 2.0 isn't available");

  // set up canvas viewport and background color
  gl.viewport(0, 0, canvas.width, canvas.height);
  gl.clearColor(bg_color, bg_color, bg_color, 1.0);

  // Load shaders
  program = initShaders(gl, "vertex-shader", "fragment-shader");
  gl.useProgram(program);

  matrixLocation = gl.getUniformLocation(program, "u_matrix");
  middlewidth = Math.floor(gl.canvas.width / 2);
  middleheight = Math.floor(gl.canvas.height / 2);

  // set up listener
  document.addEventListener("keyup", (event) => {
    var code = event.code;
    if (code === "KeyC") {
      key_charge = key_charge + 1;
      key_is_pressed = true;
    }
  });

  render();
}

function render() {
  // Draws according to the number of key press in key_charge
  gl.clear(gl.COLOR_BUFFER_BIT);
  drawBackground();
  if (key_charge > 0 && key_charge < phase1_upperbound) {
    drawPhase1();
  } else if (
    key_charge >= phase1_upperbound &&
    key_charge < phase2_upperbound
  ) {
    drawPhase2();
  } else if (key_charge >= phase2_upperbound) {
    drawPhase3();
  }

  // Check if there was a key_press, if there wasn't after a timeout,
  // reduce key_charge
  if (key_is_pressed) {
    key_is_pressed = false;
  } else {
    if (key_charge > 0) {
      key_charge = key_charge - 1;
    } else {
      key_charge = 0;
    }
  }

  // set timeout as a countdown to identify if
  // there was a key spam or not.
  setTimeout(function () {
    requestAnimationFrame(render);
  }, timeout);
}

function setGeometry(shape) {
  switch (shape) {
    case 0:
      // draw character
      return new Float32Array([
        // draw fingers of left hand
        15, 0, 0.5, 5, 0.5, 10,

        // draw thumb of left hand
        -0.5, 8, -0.5, 6, -10, 4,

        // draw left arm
        0, 10, -2, 25, 8, 35,

        // draw body
        -3, 25, -20, 40, -10, 41, -3, 25, -10, 41, -3, 35,

        // draw head
        -10, 28, -16, 23, -22, 28, -10, 28, -22, 28, -17, 33,

        // draw right arm
        -20, 40, -23, 52, -18, 45,

        // draw right arm wrist
        -23, 52, -12, 52, -20, 50,
        // draw right leg
        -8, 40, -13, 62, -8, 60,
        // draw left leg
        -3, 35, -3, 60, 3, 61,
      ]);
    case 1:
      // red lightning line
      return new Float32Array([0, -15, 5, 0, -5, 0, 0, 15]);
    case 2:
      // orange triangle
      return new Float32Array([-15, 12.975, 15, 12.99, 0, -12.99]);
    case 3:
      // draw cyan octagon, the best polygon
      return new Float32Array([
        0, 0, 5.74, -13.858, -5.74, -13.858,

        0, 0, -5.74, -13.858, -13.858, -5.74,

        0, 0, -13.858, -5.74, -13.858, 5.74,

        0, 0, -13.858, 5.74, -5.74, 13.858,

        0, 0, -5.74, 13.858, 5.74, 13.858,

        0, 0, 5.74, 13.858, 13.858, 5.74,

        0, 0, 13.858, 5.74, 13.858, -5.74,

        0, 0, 13.858, -5.74, 5.74, -13.858,
      ]);
    /*case 4: // power bar
      since power bar is dynamic, it is calculated directly in drawPowerBar function
      */
  }
}

function setColor(color) {
  switch (color) {
    case 0:
      // color for the character
      return new Uint8Array([
        // left hand fingers
        0, 0, 139, 0, 0, 139, 0, 0, 139,
        // left hand thumbs
        255, 165, 0, 255, 165, 0, 255, 165, 0,
        // left arm
        0, 0, 139, 0, 0, 139, 0, 0, 139,
        // body
        0, 0, 139, 0, 0, 139, 0, 0, 139, 255, 165, 0, 255, 165, 0, 255, 165, 0,
        // head
        255, 255, 0, 255, 255, 0, 255, 255, 0, 255, 255, 0, 255, 255, 0, 255,
        255, 0,
        // right arm
        0, 0, 139, 0, 0, 139, 0, 0, 139,
        // right arm wrist
        255, 165, 0, 255, 165, 0, 255, 165, 0,
        // right leg
        255, 165, 0, 255, 165, 0, 255, 165, 0,
        // left leg
        0, 0, 139, 0, 0, 139, 0, 0, 139,
      ]);
    case 1:
      // color for the first phase, red lightning
      return new Uint8Array([255, 0, 0, 255, 0, 0, 255, 0, 0, 255, 0, 0]);
    case 2:
      // color for the second phase, orange triangle
      return new Uint8Array([255, 165, 0, 255, 165, 0, 255, 165, 0]);
    case 3:
      // color for the third phase, cyan octagon
      return new Uint8Array([
        0, 255, 255, 0, 255, 255, 0, 255, 255, 0, 255, 255, 0, 255, 255, 0, 255,
        255, 0, 255, 255, 0, 255, 255, 0, 255, 255, 0, 255, 255, 0, 255, 255, 0,
        255, 255, 0, 255, 255, 0, 255, 255, 0, 255, 255, 0, 255, 255, 0, 255,
        255, 0, 255, 255, 0, 255, 255, 0, 255, 255, 0, 255, 255, 0, 255, 255, 0,
        255, 255, 0, 255, 255,
      ]);
    case 4:
      // color for the green power bar
      return new Uint8Array([
        0, 255, 0, 0, 255, 0, 0, 255, 0, 0, 255, 0, 0, 255, 0, 0, 255, 0, 0,
        255, 0, 0, 255, 0, 0, 255, 0, 0, 255, 0, 0, 255, 0, 0, 255, 0,
      ]);
    case 5:
      // color for the yellow power bar
      return new Uint8Array([
        255, 255, 0, 255, 255, 0, 255, 255, 0, 255, 255, 0, 255, 255, 0, 255,
        255, 0, 255, 255, 0, 255, 255, 0, 255, 255, 0, 255, 255, 0, 255, 255, 0,
        255, 255, 0,
      ]);
    case 6:
      // color for the red power bar
      return new Uint8Array([
        255, 0, 0, 255, 0, 0, 255, 0, 0, 255, 0, 0, 255, 0, 0, 255, 0, 0, 255,
        0, 0, 255, 0, 0, 255, 0, 0, 255, 0, 0, 255, 0, 0, 255, 0, 0,
      ]);
  }
}

/* Create buffers for multiple objects */
function initBuffers(vertices) {
  var n = vertices.length / 2;
  var vertexBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);

  var vPosition = gl.getAttribLocation(program, "vPosition");
  gl.vertexAttribPointer(vPosition, 2, gl.FLOAT, false, 0, 0);
  gl.enableVertexAttribArray(vPosition);
  return n;
}

/* Create color buffers for multiple objects */
function initColorBuffers(vertices) {
  var n = vertices.length / 3;
  var colorBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);

  var colorLocation = gl.getAttribLocation(program, "vColor");
  gl.vertexAttribPointer(colorLocation, 3, gl.UNSIGNED_BYTE, true, 0, 0);
  gl.enableVertexAttribArray(colorLocation);
}

// var m3 from Angel Book
var m3 = {
  //setup 3x3 transformation matrix object
  identity: function () {
    return [1, 0, 0, 0, 1, 0, 0, 0, 1];
  },

  projection: function (width, height) {
    // Note: This matrix flips the Y axis so that 0 is at the top.
    return [2 / width, 0, 0, 0, -2 / height, 0, -1, 1, 1];
  },

  translation: function (tx, ty) {
    return [1, 0, 0, 0, 1, 0, tx, ty, 1];
  },

  rotation: function (angleInRadians) {
    var c = Math.cos(angleInRadians);
    var s = Math.sin(angleInRadians);
    return [c, -s, 0, s, c, 0, 0, 0, 1];
  },

  scaling: function (sx, sy) {
    return [sx, 0, 0, 0, sy, 0, 0, 0, 1];
  },

  multiply: function (a, b) {
    var a00 = a[0 * 3 + 0];
    var a01 = a[0 * 3 + 1];
    var a02 = a[0 * 3 + 2];
    var a10 = a[1 * 3 + 0];
    var a11 = a[1 * 3 + 1];
    var a12 = a[1 * 3 + 2];
    var a20 = a[2 * 3 + 0];
    var a21 = a[2 * 3 + 1];
    var a22 = a[2 * 3 + 2];
    var b00 = b[0 * 3 + 0];
    var b01 = b[0 * 3 + 1];
    var b02 = b[0 * 3 + 2];
    var b10 = b[1 * 3 + 0];
    var b11 = b[1 * 3 + 1];
    var b12 = b[1 * 3 + 2];
    var b20 = b[2 * 3 + 0];
    var b21 = b[2 * 3 + 1];
    var b22 = b[2 * 3 + 2];
    return [
      b00 * a00 + b01 * a10 + b02 * a20,
      b00 * a01 + b01 * a11 + b02 * a21,
      b00 * a02 + b01 * a12 + b02 * a22,
      b10 * a00 + b11 * a10 + b12 * a20,
      b10 * a01 + b11 * a11 + b12 * a21,
      b10 * a02 + b11 * a12 + b12 * a22,
      b20 * a00 + b21 * a10 + b22 * a20,
      b20 * a01 + b21 * a11 + b22 * a21,
      b20 * a02 + b21 * a12 + b22 * a22,
    ];
  },
};
init();

Soal 1 UTS: Charge Attack!
Charge Attack! adalah sebuah animasi 2D interaktif dimana pengguna dapat menirukan sebuah "Charge Attack".

Struktur direktori:
> library Common:
	> webgl-utils.js: standard utilities from google to set up a webgl context

	> MV.js: our matrix/vector package. Documentation on website

	> initShaders.js: functions to initialize shaders in the html file

	> initShaders2.js: functions to initialize shaders that are in separate files
> chargeAttack.html
> chargeAttack.js

Petunjuk Penggunaan:
Spam key "C" di keyboard dan animasi akan berjalan.

Kontribusi anggota kelompok:
1. Muhammad Tsaqif Al Bari - 1806235731
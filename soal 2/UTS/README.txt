Jawaban No. 2 UTS
Menggambar garis menggunakan poin-poin yang digenerate menggunakan algoritma midpoint.
Algoritma midpoint yang digunakan dapat membuat garis dengan semua kasus slope.

Petunjuk penggunaan:
1. Klik 1 kali dikanvas untuk meletakkan koordinat pertama.
2. Klik 1 kali lagi dikanvas untuk meletakkan koordinat kedua lalu garis akan segera di render.

Kontribusi No.2:
1. Faza Aulia Aryoga (100%)
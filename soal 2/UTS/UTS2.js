"use strict";

var canvas;
var gl;

var maxNumPositions = 2000;
var index = 0;

var cindex = 0;

var colors = [
  vec4(0.0, 0.0, 0.0, 1.0), // black
  vec4(1.0, 0.0, 0.0, 1.0), // red
  vec4(1.0, 1.0, 0.0, 1.0), // yellow
  vec4(0.0, 1.0, 0.0, 1.0), // green
  vec4(0.0, 0.0, 1.0, 1.0), // blue
  vec4(1.0, 0.0, 1.0, 1.0), // magenta
  vec4(0.0, 1.0, 1.0, 1.0), // cyan
];
var t, tt;
var numPolygons = 0;
var numPositions = [];
numPositions[0] = 0;
var start = [0];
var currentX, currentY, currentX2, currentY2;
var pointCoords = [];

function init() {
  canvas = document.getElementById("gl-canvas");

  gl = canvas.getContext("webgl2");
  if (!gl) alert("WebGL 2.0 isn't available");

  var m = document.getElementById("mymenu");

  gl.viewport(0, 0, canvas.width, canvas.height);
  gl.clearColor(0.8, 0.8, 0.8, 1.0);
  gl.clear(gl.COLOR_BUFFER_BIT);
  //
  //  Load shaders and initialize attribute buffers
  //
  var program = initShaders(gl, "vertex-shader", "fragment-shader");
  gl.useProgram(program);

  var bufferId = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, bufferId);
  gl.bufferData(gl.ARRAY_BUFFER, 8 * maxNumPositions, gl.STATIC_DRAW);

  var postionLoc = gl.getAttribLocation(program, "aPosition");
  gl.vertexAttribPointer(postionLoc, 2, gl.FLOAT, false, 0, 0);
  gl.enableVertexAttribArray(postionLoc);

  var cBufferId = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, cBufferId);
  gl.bufferData(gl.ARRAY_BUFFER, 16 * maxNumPositions, gl.STATIC_DRAW);

  var colorLoc = gl.getAttribLocation(program, "aColor");
  gl.vertexAttribPointer(colorLoc, 4, gl.FLOAT, false, 0, 0);
  gl.enableVertexAttribArray(colorLoc);

  m.addEventListener("click", function () {
    cindex = m.selectedIndex;
  });

  canvas.addEventListener("mousedown", function (event) {
    if (index == 0) { // Ambil koordinat pertama
      currentX = event.clientX;
      currentY = canvas.height - event.clientY;
      index++;
    } else { // Ambil koordinat kedua, hitung titik-titik lalu render
        currentX2 = event.clientX;
        currentY2 = canvas.height - event.clientY;
        index = 0;
        pointCoords = [];
        midPoint(currentX, currentY, currentX2, currentY2);

        gl.bindBuffer(gl.ARRAY_BUFFER, bufferId);
        for (var i = 0; i < pointCoords.length - 1; i++)
          gl.bufferSubData(gl.ARRAY_BUFFER, 8 * i, flatten(pointCoords[i]));

        gl.bindBuffer(gl.ARRAY_BUFFER, cBufferId);
        var currentColor = vec4(colors[cindex]);
        for (var i = 0; i < pointCoords.length - 1; i++)
          gl.bufferSubData(gl.ARRAY_BUFFER, 16 * i, flatten(currentColor));
        render();
    }
  });
}

function render() {
  gl.clear(gl.COLOR_BUFFER_BIT);
  gl.drawArrays(gl.POINTS, 0, pointCoords.length - 1);
}

function convertCoordX(int1) {
  var x = (2 * int1) / canvas.width - 1;
  return x;
}

function convertCoordY(int1) {
  var y = (2 * int1) / canvas.height - 1;
  return y;
}

function midPoint(x1, y1, x2, y2) {
  // Swap koordinat jika terbalik
  if (x1 > x2) {
    var tmp1, tmp2;
    tmp1 = x1;
    tmp2 = y1;
    x1 = x2;
    y1 = y2;
    x2 = tmp1;
    y2 = tmp2;
  }
  var dx = x2 - x1;
  var dy = y2 - y1;
  var m = dy / dx;

  var xi = x1;
  var yi = y1;
  var x = convertCoordX(xi);
  var y = convertCoordY(yi);
  pointCoords.push(vec2(x, y));

  if (m > 0 && m <= 1) { // Jika 0 < slope(m) <=1 
    var D = 2 * dy - dx;
    var incE = 2 * dy;
    var incNE = 2 * (dy - dx);
    while (xi < x2) {
      if (D < 0) {
        D = D + incE;
      } else {
        D = D + incNE;
        yi++;
      }
      xi++;
      x = convertCoordX(xi);
      y = convertCoordY(yi);
      pointCoords.push(vec2(x, y));
    }
  } else if (m > 1) { // Jika slope(m) > 1 
    var D = dy - 2 * dx;
    var incN = -2 * dx;
    var incNE = 2 * (dy - dx);
    while (yi < y2) {
      if (D <= 0) {
        D = D - incN;
      } else {
        D = D - incNE;
        xi++;
      }
      yi++;
      x = convertCoordX(xi);
      y = convertCoordY(yi);
      pointCoords.push(vec2(x, y));
    }
  } else if (m <= 0 && m > -1) { // Jika 0 >= slope(m) > -1 
    var D = dx + 2 * dy;
    var incE = 2 * dy;
    var incSE = 2 * (dy + dx);
    while (xi < x2) {
      if (D >= 0) {
        D = D + incE;
      } else {
        D = D + incSE;
        yi--;
      }
      xi++;
      x = convertCoordX(xi);
      y = convertCoordY(yi);
      pointCoords.push(vec2(x, y));
    }
  } else if (m < -1) { // Jika slope(m) < -1 
    var D = 2 * dx + dy;
    var incN = 2 * dx;
    var incNE = 2 * (dx + dy);
    while (yi > y2) {
      if (D <= 0) {
        D = D + incN;
      } else {
        D = D + incNE;
        xi++;
      }
      yi--;
      x = convertCoordX(xi);
      y = convertCoordY(yi);
      pointCoords.push(vec2(x, y));
    }
  }
}

init();
